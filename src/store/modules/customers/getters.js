const getters = {
  allCustomers: state => state.customers,
  getCustomer: state => state.customer,
  // check if an object contains values
  isLoaded: state => !!Object.values(state.customers).length
};

export default getters;
